+++
date = "2016-11-05T21:05:33+05:30"
title = "About"
+++

I made this website to help me keep track of the books I have read. 

Its built with [hugo](https://gohugo.io/), and pulls all of the images, book metadata, and previews from the [Google Books api](https://developers.google.com/books).
