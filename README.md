# Josh Reads Books
## Demo

[![Thumbnail](pics/joshreadsbooks.jpg)](https://books.joshdavies.dev "JoshReadsBooks - Click to open website")
[Click image (or here) to open website](https://books.joshdavies.dev)

## Description
Josh Reads Books is a Hugo website I created to help keep track of the books I have read.
I tweaked the template a little bit, and included some golang scripts that pull book information
from the google books api.

Major features completed are:
* Terraformed in AWS (google domains -> cloudfront -> s3)
* Uses a structured list of books in a text file to grab book information from Google Books api
* About view with book count and pages count
* Main view with books
* Details view for each book

Other features I may implement in the future are:
* Adding my notes/ratings for specific books
* Searching based on book name/author name
* Tags

## Running Locally
`go mod init scripts` from root dir

### Adding a Book
* Update `scripts/bookNames.txt` with the book name
* Run the preprocess script: `go run . > errors.txt`
* If there are errors, use the [google books api](https://books.google.com) to find the book
* Two files will be generated, a content/portfolio/<bookName>.md and a data/books/<bookName>.json
* Build the website: `hugo -D`
* Push the website `aws s3 sync public s3://books.joshdavies.dev/`
* View changes to the [site](https://books.joshdavies.dev)

### Notes
Disable the medium thumbnail by just changing the json key for medium under the `data/books` json 
file if it looks bad

Add any notes about the book or a rating to the markdown under the `content/portfolio` md file

## Attribution
I did not write the Hugo Theme, but instead tweaked one provided [here](https://github.com/kishaningithub/hugo-creative-portfolio-theme)
