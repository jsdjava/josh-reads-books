resource "aws_cloudfront_origin_access_identity" "joshreadsbooks" {
}
resource "aws_cloudfront_distribution" "joshreadsbooks" {
  origin {
    domain_name = "books.joshdavies.dev.s3-website-us-east-1.amazonaws.com"
    origin_id   = "s3"
    s3_origin_config {
      origin_access_identity =  aws_cloudfront_origin_access_identity.joshreadsbooks.cloudfront_access_identity_path
    }
  }
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"
  aliases = ["books.joshdavies.dev"]
  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "s3"    
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 300
    default_ttl            = 300
    max_ttl                = 300
    
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.joshdavies.arn
    ssl_support_method = "sni-only"
  }
  price_class = "PriceClass_All"
}