resource "aws_acm_certificate" "joshdavies" {
  domain_name = "books.joshdavies.dev"
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}