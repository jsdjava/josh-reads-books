resource "aws_s3_bucket" "joshreadsbooks" {
  bucket = "books.joshdavies.dev"
  
  website {
    index_document = "index.html"
    error_document = "404.html"
  }
}

resource "aws_s3_bucket_policy" "joshreadsbooks-s3-policy" {
  bucket = "books.joshdavies.dev"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicRead",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion"
            ],
            "Resource": "arn:aws:s3:::books.joshdavies.dev/*"
        }
    ]
}
POLICY
}
