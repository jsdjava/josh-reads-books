// write all the book information into the data folder using the books.txt file
// structure is data/bookName/bookSearchJson of each book result
// also, create a page in content/portfolio IF it does not exist
// add a partial to that page with a variable (bookSearchJson) that fills the not overriden stuff out

package main

import (
   "io/ioutil"
   "log"
   "net/http"
   "net/url"
   "strings"
   "fmt"
   "github.com/tidwall/gjson"
   "os"
   "regexp"
   "text/template"
   "math"
   "errors"
)

func QuitIfError(err error){
   if err != nil{
      log.Fatal(err);
   }
}

func ReadBookNames() []string {
  content,err := ioutil.ReadFile("bookNames.txt");
  QuitIfError(err)
  rawNames := strings.Split(strings.TrimSpace(string(content)),"\r\n")
  for i,s := range rawNames {
     if !strings.HasPrefix(s,"volumeId="){
       rawNames[i] = strings.ToLower(s)
     }
  }
  return rawNames
}

// kind of a low-tech hamming distance
func AreSimiliarEnough(a string, b string) bool{
  r := regexp.MustCompile("[^0-9A-Za-z]")
  cleanedA := r.ReplaceAllString(strings.ToLower(a),"")
  cleanedB := r.ReplaceAllString(strings.ToLower(b),"")
  diffCount := 0
  length := int(math.Max(float64(len(cleanedA)),float64(len(cleanedB))))
  for i:=0; i<length; i++{
    if (i>=len(cleanedA) || i>=len(cleanedB) || cleanedA[i] != cleanedB[i]){
      diffCount++;
    }
  }
  return float64(diffCount)/float64(length) < float64(0.2)
}

func GetBookInfo(bookName string,volumeId string) string{
   bookSearchUrl := fmt.Sprintf("https://www.googleapis.com/books/v1/volumes?q=%s&maxResults=10&startIndex=0",url.QueryEscape(bookName))
   resp, err := http.Get(bookSearchUrl)
   QuitIfError(err)
   body,err := ioutil.ReadAll(resp.Body)
   QuitIfError(err)
   volumes := gjson.Get(string(body),"items")
   misses := make([]string,0)
   hasImage := false
   if volumeId == ""{
      volumes.ForEach(func(key,value gjson.Result) bool{
      // prioritize ones that
      // - have a preview
      // - have a thumbnail
      // - match "closest" to the text of the title (only if they don't match at all)
      actualTitle := value.Get("volumeInfo.title").String()+value.Get("volumeInfo.subtitle").String()
      actualTitleAndAuthor := actualTitle + value.Get("volumeInfo.authors.0").String()
      if AreSimiliarEnough(actualTitle,bookName) || AreSimiliarEnough(actualTitleAndAuthor,bookName) {
         if !hasImage {
         volumeId = value.Get("id").String()
         }
         if value.Get("volumeInfo.imageLinks.thumbnail").Exists() {
            hasImage = true;
            volumeId = value.Get("id").String()
            if(value.Get("accessInfo.accessViewStatus").String() != "NONE"){
               return false;
            }
         }
      } else{
         misses = append(misses,actualTitle)
         misses = append(misses,actualTitleAndAuthor)
      }
      return true;
      })
      if volumeId == "" {
         fmt.Printf("Could not find any matches for %s\n",bookName)
         for _,miss := range misses{
            fmt.Printf("%s,",miss)
         }
      }
   }
   bookUrl := fmt.Sprintf("https://www.googleapis.com/books/v1/volumes/%s",volumeId)
   resp, err = http.Get(bookUrl)
   QuitIfError(err)
   body,err = ioutil.ReadAll(resp.Body)
   QuitIfError(err)
   return string(body)
}

func WriteBookData(bookName string, bookInfo string){
   r := regexp.MustCompile("[^0-9A-Za-z]+")
   fileName := fmt.Sprintf("../data/books/%s.json",r.ReplaceAllString(bookName,"-"))
   if _, err := os.Stat(fileName); errors.Is(err, os.ErrNotExist) {
      file, err := os.Create(fileName)
      QuitIfError(err)
      defer file.Close()
      file.WriteString(bookInfo)
   }
}

type BookPage struct{
  Title string
  Date string
}

func WriteDefaultBookPage(bookName string){
   r := regexp.MustCompile("[^0-9A-Za-z]+")
   bookPage := BookPage{
      Title: r.ReplaceAllString(bookName,"-"),
      Date: "test",
   }
   fileName := fmt.Sprintf("../content/portfolio/%s.md",bookPage.Title)
   if _, err := os.Stat(fileName); errors.Is(err, os.ErrNotExist) {
      file, err := os.Create(fileName)
      QuitIfError(err)
      defer file.Close()
      templateText, err := ioutil.ReadFile("bookTemplate.md")
      QuitIfError(err)
      tmpl, err := template.New("Book Template").Parse(string(templateText))
      QuitIfError(err)
      err = tmpl.Execute(file,bookPage)
      QuitIfError(err)
   }
}

func WriteBookStats(numBooks int, numPages int){
   fileName := "../data/stats.json"
   file, err := os.Create(fileName)
   QuitIfError(err)
   defer file.Close()
   file.WriteString(fmt.Sprintf(`{"numBooks":%d,"numPages":%d}`,numBooks,numPages))
}

func main() {
   bookNames := ReadBookNames();
   numPages := int(0);
   numBooks := len(bookNames)
   for _,bookName := range bookNames {
      // sometimes, i want a certain book edition or cover or
      // the search just isn't finding the book, so i override with the volumeId
      // of the search result I like
      volumeId :=  ""
      if strings.HasPrefix(bookName,"volumeId="){
         bookPieces := strings.SplitN(strings.SplitN(bookName,"volumeId=",2)[1]," ",2)
         volumeId = bookPieces[0]
         bookName = strings.ToLower(bookPieces[1])
      }
      bookInfo := GetBookInfo(bookName,volumeId)
      numPages += int(gjson.Get(bookInfo,"volumeInfo.pageCount").Int())
      WriteBookData(bookName,bookInfo)
      WriteDefaultBookPage(bookName)
   }
   WriteBookStats(numBooks,numPages)
}
